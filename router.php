<?php

error_reporting(E_ERROR | E_PARSE);

//fetch registered services
$registry = json_decode(file_get_contents('conf/service_registry.json'), true);

//check for service if registered or not
$service = (trim($_SERVER['REQUEST_URI']) != '/') ? $_SERVER['REQUEST_URI'] : '/default';
$path_of_service = (array_key_exists($service, $registry)) ? $registry[$service] : 'error';

//retrieve json object binded in URL
$request_data = json_decode(file_get_contents('php://input'),true);



if ($path_of_service != 'error') {
    include_once('lib/database_connection.php');
    include_once('/lib/cache_memcache.php');
    include_once($path_of_service);
    $data = '';
    $obj = new Service();
    $responce = $obj->doService($request_data);

    //setting environment for json return
    header('Content-type: application/json');
    echo json_encode($responce);
} else {
    include_once('error.php');
}

?>