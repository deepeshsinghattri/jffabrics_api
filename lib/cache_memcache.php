<?php

/**
 * Created by PhpStorm.
 * User: Moulali
 * Date: 1/19/2016
 * Time: 6:38 PM
 */
class cacheMemcache
{
    var $iTime = 600; //Time to token expiry
    var $bEnabled = false; // Memcache enabled?
    var $oCache = null;

    // constructor
    function __construct() {
                if (class_exists('Memcache')) {
                    $this->oCache = new Memcache();
                    $this->bEnabled = true;
            if (! $this->oCache->connect('localhost', 11211)){
                $this->oCache = null;
                $this->bEnabled = false;
            }
        }
    }
    // get data from cache server
    function getData($sKey) {
        $vData = $this->oCache->get($sKey);
        return false === $vData ? null : $vData;
    }

    // save data to cache server
    function setData($sKey, $vData,$expiryTime ) {
        if(($expiryTime>0)&&(is_numeric($expiryTime))){ $this->iTime=$expiryTime;}
        return $this->oCache->set($sKey, $vData,MEMCACHE_COMPRESSED,$this->iTime);
    }

    // delete data from cache server
    function delData($sKey) {
        return $this->oCache->delete($sKey);
    }

    // get data from cache server
    function isKeyExists($sKey) {
        $vData = $this->oCache->get($sKey);
        return false === $vData ? false : true;
    }


}