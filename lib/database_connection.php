<?php
include('cache_memcache.php');
class database_connection extends cacheMemcache {

    public function connect() {

        $data_source_file = file_get_contents('conf/datasource.json');
        $data_source_obj = (object)json_decode($data_source_file, true);

        $conn_string = "DRIVER={iSeries Access ODBC Driver};" .
            "SYSTEM=$data_source_obj->hostname;" .
            "DATABASE=$data_source_obj->database;" .
            "HOSTNAME=$data_source_obj->hostname;" .
            "PORT=$data_source_obj->port;" .
            "PROTOCOL=TCPIP;";

        try{
            $conn = odbc_connect($conn_string, $data_source_obj->username, $data_source_obj->password);
        }catch (Exception $e){
            return false;
        }

        return $conn;
    }

    public function isAuthorizedKeys($key) {

        $data_source_file = file_get_contents('conf/datasource.json');
        $data_source_obj = (object)json_decode($data_source_file, true);

        $keys[] = $data_source_obj->web_key;
        $keys[] = $data_source_obj->mb_key;

        $result = (in_array($key,$keys))?true:false;

        return $result;
    }

    public function getRandomNumber() {

        $length = 8;
        $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%^&*';

        $crypto_rand_secure = function ( $min, $max ) {
            $range = $max - $min;
            if ( $range < 0 ) return $min; // not so random...
            $log    = log( $range, 2 );
            $bytes  = (int) ( $log / 8 ) + 1; // length in bytes
            $bits   = (int) $log + 1; // length in bits
            $filter = (int) ( 1 << $bits ) - 1; // set all lower bits to 1
            do {
                $rnd = hexdec( bin2hex( openssl_random_pseudo_bytes( $bytes ) ) );
                $rnd = $rnd & $filter; // discard irrelevant bits
            } while ( $rnd >= $range );
            return $min + $rnd;
        };

        $token = "";
        $max   = strlen( $pool );
        for ( $i = 0; $i < $length; $i++ ) {
            $token .= $pool[$crypto_rand_secure( 0, $max )];
        }
        return $token;
    }
}