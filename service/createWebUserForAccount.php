<?php

class Service extends database_connection
{
    public function doService($data)
    {
        //output json message parameters,
        $record['fault']=false;            //Only indicates application internal issues. For example, if database server is down
        $record['result']=false;           //True if the account was validated, false otherwise.
        $record['user_id']='';             //User id of user, if result is true, undefined otherwise.
        $record['jfauth_token']='';        //Set to the token, if result is true, undefined otherwise.
        $record['fault_code']=0;           //if there is problem in database connection, default 0 if no issue.
        $record['result_code']=0;           //if there is problem in application, default 0 if no issue.


        if(!$this->isAuthorizedKeys($data['key'])){
            $record['result_code']=60;      //60 is for authorize key missing
            return (object)$record;
        }

        $conn = $this->connect();
        if(!$conn){
            $record['fault']=true;
            $record['fault_code']=11;
            return (object)$record;
        }

        $ranNumber = $this->getRandomNumber();
        $where = '';
        if((trim($data['user_id'])!='')&&(trim($data['email'])!='')){
            $where = " where WEB_USER_ID='".$data['user_id']."'";

            $query = "SELECT WEB_ACCOUNT_NO from DEMO_DAY.WEBUSR ".$where;
            $result1 = odbc_exec($conn, $query);

            $row1 = odbc_fetch_array($result1);

            if(trim($row1['WEB_ACCOUNT_NO'])==''){

                $where = " where WEB_EMAIL_ADDRESS='".$data['email']."'";

                $query = "SELECT WEB_ACCOUNT_NO from DEMO_DAY.WEBUSR ".$where;
                $result2 = odbc_exec($conn, $query);

                $row2 = odbc_fetch_array($result2);

                if(trim($row2['WEB_ACCOUNT_NO'])==''){

                    $date = date("Y-m-d");
                    $query = "INSERT INTO DEMO_DAY.WEBUSR
                              VALUES ('".$data['user_id']."','".$data['password']."','".$data['account_number']."','".$data['email']."','".$date."') with NC";

                    $result = odbc_exec($conn, $query) or die("<p>".odbc_errormsg());


                    $record['result'] = true;
                    $record['user_id'] = $data['user_id'];
                    $record['jfauth_token'] = md5($data['account_number'].$ranNumber);

                }else{
                    $record['result_code']=59;//if already exists
                }
            }else{
                $record['result_code']=59;//if already exists
            }
        }else{
            $record['result_code']=51;//parameter missing
        }



        odbc_close($conn);
        return (object)$record;
    }
}

?>