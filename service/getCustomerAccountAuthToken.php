<?php

class Service extends database_connection
{
    public function doService($data)
    {
        //output json message parameters,
        $record['fault']=false;            //Only indicates application internal issues. For example, if database server is down
        $record['result']=false;           //True if the account was validated, false otherwise.
        $record['jfauth_token']='';        //Set to the token, if result is true, undefined otherwise.
        $record['fault_code']=0;           //if there is problem in database connection, default 0 if no issue.
        $record['result_code']=0;           //if there is problem in application, default 0 if no issue.



        if(!$this->isAuthorizedKeys($data['key'])){
            $record['result_code']=60;      //60 is for authorize key missing
            return (object)$record;
        }

        $conn = $this->connect();
        if(!$conn){
            $record['fault']=true;
            $record['fault_code']=11;
            return (object)$record;
        }

        $ranNumber = $this->getRandomNumber();
        $where = '';
        if((trim($data['account_number'])!='')&&(trim($data['email'])!='')){
            $where = " where ARC_CUST_NO='".$data['account_number']."' AND ARC_EMAIL='".$data['email']."'";

            $query = "SELECT ARC_CUST_NO,ARC_POST_CODE,ARC_EMAIL from DEMO_DAY.ARCUST ".$where;
            $result = odbc_exec($conn, $query);

            $row = odbc_fetch_array($result);

            if(trim($row['ARC_CUST_NO'])!=''){
                $record['result'] = true;
                $record['jfauth_token'] = md5($data['account_number'].$ranNumber);
                $oCache = new CacheMemcache();
                if ($oCache->bEnabled){}
                    $oCache->setData($record['jfauth_token'], $data['account_number']);
                    $memData = $oCache->getData($record['jfauth_token']);
            }else{
                $record['result_code']=52;//if not exists
            }
        }else{
            $record['result_code']=51;//parameter missing
        }

        odbc_close($conn);
        return (object)$record;
    }
}

?>