<?php

class Service extends database_connection
{
    public function doService($data)
    {
        //output json message parameters,
        $record['fault']=false;            //Only indicates application internal issues. For example, if database server is down
        $record['result']=false;           //True if the account was validated, false otherwise.
        $record['fault_code']=0;           //if there is problem in database connection, default 0 if no issue.
        $record['result_code']=0;           //if there is problem in application, default 0 if no issue.

        if(!$this->isAuthorizedKeys($data['key'])){
            $record['result_code']=60;      //60 is for authorize key missing
            return (object)$record;
        }

        // this will check whether token exists or not
        if(!$this->isKeyExists($data['token'])){
            $record['result_code']=54;      //54 is for token missing
            return (object)$record;
        }

        $conn = $this->connect();
        if(!$conn){
            $record['fault']=true;
            $record['fault_code']=11;
            return (object)$record;
        }

        $where = '';
        if((trim($data['token'])!='')&&((trim($data['pattern_name_search'])!='')||(trim($data['book_id'])!=''))){

            $where = " where A.PTN_DATE_INTROD>'0001-01-01'
                            AND A.PTN_CANCEL_DATE!='0001-01-01'
                            AND PTN_PATTERN=STD_PATTERN ";

            if($data['hide_domestic_only']==true){
                $where .= " AND A.PTN_DOMESTIC!='Y' ";
            }
            if(trim($data['book_id'])!=''){
                $where .= " AND D.PTBK_SMPL_BOOK_NO='$data[book_id]' ";
            }
            if(trim($data['pattern_name_search'])!=''){
                $where .= " AND A.PTN_PATTERN like '%$data[pattern_name_search]%' ";
            }

            $query = "SELECT DISTINCT A.PTN_PATTERN, B.STd_COLOuR, D.PTBK_SMPL_BOOK_NO, A.PTN_DATE_INTROD, B.CLF_CANCEL_DATE , A.PTN_CATEGORY
                      from DEMO_DAY.STPATN as A, DEMO_DAY.STCLFL as B, DEMO_DAY.SLPTBK as D ".$where;
            $result = odbc_exec($conn, $query);

            $pattern = '';
            $i=0;
            while($row = odbc_fetch_array($result)){
                $i++;
                if($pattern==$row['PTN_PATTERN']){
                    $color_code[]   = $row['STD_COLOUR'];
                }else{
                    $fields['pattern_name'] = $row['PTN_PATTERN'];
                    $fields['book_id']      = $row['PTBK_SMPL_BOOK_NO'];
                    $fields['status']       = (trim($row['CLF_CANCEL_DATE']!='0001-01-01'))?'1':'0';
                    $fields['category']     = $row['PTN_CATEGORY'];
                    if($data['include_color_codes']==true)
                    $fields['color_code']   = $color_code;
                    $record['data'][]       = $fields;
                    unset($fields);
                    unset($color_code);
                }
                $pattern=$row['PTN_PATTERN'];
            }

            if($i>0){
                $record['result'] = true;
            }else{
                $record['result_code']=52;
            }

        }else{
            $record['result_code']=51;
        }

        odbc_close($conn);
        return (object)$record;
    }
}

?>