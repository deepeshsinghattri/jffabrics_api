<?php

class Service extends database_connection
{
    public function doService($data)
    {
        //output json message parameters,
        $record['fault'] = false;            //Only indicates application internal issues. For example, if database server is down
        $record['result'] = false;           //True if the account was validated, false otherwise.
        $record['fault_code'] = 0;           //True if the account was validated, false otherwise.
        $record['result_code'] = 0;           //True if the account was validated, false otherwise.


        if (!$this->isAuthorizedKeys($data['key'])) {
            $record['result_code'] = 60;      //60 is for authorize key missing
            return (object)$record;
        }

        // this will check whether token exists or not
        if (!$this->isKeyExists($data['token'])) {
            $record['result_code'] = 54;      //54 is for token missing
            return (object)$record;
        }

        $conn = $this->connect();
        if (!$conn) {
            $record['fault'] = true;
            $record['fault_code'] = 11;
            return (object)$record;
        }

        $where = '';
        if((trim($data['token'])!='')&& (trim($data['order_id'])!='')){
            //print_r('order_id');
            $where = "where OEL_ORDER_NO ='$data[order_id]'";

            $query = "SELECT OEL_ORDER_NO,OEL_DESC from DEMO_DAY.OELINE ".$where;
            $result = odbc_exec($conn,$query);
            while($row = odbc_fetch_array($result)){
                $record['data']['order_id'] = $row['OEL_ORDER_NO'];
                $record['data']['order_items'][] = trim($row['OEL_DESC']);
            }
            if(count($record['data'])){
                $record['result'] = true;
            }else{
                $record['result_code']=52;
            }
        }else{
            $record['result_code']=51;
        }
        odbc_close($conn);
        return (object)$record;
    }
}