<?php

class Service extends database_connection
{
    public function doService($data)
    {
        //output json message parameters,
        $record['fault']=false;            //Only indicates application internal issues. For example, if database server is down
        $record['result']=false;           //True if the account was validated, false otherwise.
        $record['fault_code']=0;           //True if the account was validated, false otherwise.
        $record['result_code']=0;           //True if the account was validated, false otherwise.


        if(!$this->isAuthorizedKeys($data['key'])){
            $record['result_code']=60;      //60 is for authorize key missing
            return (object)$record;
        }

        // this will check whether token exists or not
        if(!$this->isKeyExists($data['token'])){
            $record['result_code']=54;      //54 is for token missing
            return (object)$record;
        }

        $conn = $this->connect();
        if(!$conn){
            $record['fault']=true;
            $record['fault_code']=11;
            return (object)$record;
        }

        $where = '';
        if((trim($data['token'])!='')&&(trim($data['pattern_name'])!='')&&(trim($data['book_id'])!='')){
            $where = " where PTN_PATTERN=STD_PATTERN AND STD_PATTERN=PTBK_PATTERN and STD_COLOUR=PTBK_COLOUR  ";

            $where .= " AND A.PTN_PATTERN='$data[pattern_name]' AND D.PTBK_SMPL_BOOK_NO='$data[book_id]' ";

            $query = "SELECT DISTINCT A.PTN_PATTERN, B.STd_COLOuR, D.PTBK_SMPL_BOOK_NO, A.PTN_DATE_INTROD, B.CLF_CANCEL_DATE, concat(B.STd_COLOuR,D.PTBK_SMPL_BOOK_NO) as full_colour_code
                      from DEMO_DAY.STPATN as A, DEMO_DAY.STCLFL as B, DEMO_DAY.SLPTBK as D".$where;
            $result = odbc_exec($conn, $query);

            while($row = odbc_fetch_array($result)){
                $fields['color_code']   = $row['STD_COLOUR'];
                $fields['book_id']      = $row['PTBK_SMPL_BOOK_NO'];
                $fields['status']       = (trim($row['CLF_CANCEL_DATE']!='0001-01-01'))?'1':'0';
                $record['data'][]       = $fields;
            }

            if(trim($row['PTN_PATTERN'])!=''){
                $record['result'] = true;
            }else{
                $record['result_code']=52;
            }

        }else{
            $record['result_code']=51;
        }

        odbc_close($conn);
        return (object)$record;
    }
}

?>