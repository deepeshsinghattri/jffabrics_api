<?php

class Service extends database_connection
{
    public function doService($data)
    {
        //output json message parameters,
        $record['fault'] = false;            //Only indicates application internal issues. For example, if database server is down
        $record['result'] = false;           //True if the account was validated, false otherwise.
        $record['fault_code'] = 0;           //True if the account was validated, false otherwise.
        $record['result_code'] = 0;          //True if the account was validated, false otherwise.

        if (!$this->isKeyExists($data['token'])) {
            $record['result_code'] = 54;
            return (object)$record;
        }

        $conn = $this->connect();
        if (!$conn) {
            $record['fault'] = true;
            $record['fault_code'] = 11;
            return (object)$record;

        }
        $where = '';
        if ((trim($data['account_number']) != '') ) {

            $where = " where ARC_CUST_NO ='".$data['account_number']."'";

            $query = "SELECT ARC_COMPANY_NO,ARC_PRICE_CODE from DEMO_DAY.ARCUST" . $where;
            $result = odbc_exec($conn,$query);
            $row = odbc_fetch_array($result);

            if (trim($row['ARC_COMPANY_NO']) != '') {
                $record['result'] = true;
                $record['price_code'] = $row['ARC_PRICE_CODE'];
            }else{
                $record['result_code']=52;
            }
        }else{
            $record['result_code'] = 51;
        }
        odbc_close($conn);
        return (object)$record;
    }
}