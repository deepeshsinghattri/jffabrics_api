<?php

class Service extends database_connection
{
    public function doService($data)
    {
        //output json message parameters,
        $record['fault']=false;            //Only indicates application internal issues. For example, if database server is down
        $record['result']=false;           //True if the account was validated, false otherwise.
        $record['fault_code']=0;           //True if the account was validated, false otherwise.
        $record['result_code']=0;          //True if the account was validated, false otherwise.
        //$record['category']='';            //category of user, if result is true, undefined otherwise.
        //$record['jfauth_token']='';        //Set to the token, if result is true, undefined otherwise.

        if(!$this->isKeyExists($data['token'])) {
            $record['result_code']=54;
            return (object)$record;
        }

        $conn = $this->connect();
        if(!$conn){
            $record['fault']=true;
            $record['fault_code']=11;
            return (object)$record;

        }
        $where = '';
        if (((trim($data['all_books']) == true)&&(trim($data['company']) != '') )|| ((trim($data['cancelled']) == true)&& (trim($data['company']) != '')) ||( (trim($data['current']) == true))&&(trim($data['company']) != '')) {

            $where = "where ";
            if((trim($data['all_books']) == true)&&(trim($data['company']) != '') )
                $where .= " (A.SLB_DATE_INTRO != '01/01/1900' or  A.SLB_DATE_INTRO != '01/01/0001' )
                AND A.SLB_CUSTOM_BOOK != 'Y'
                AND NOT A.SLB_DESC like '%JC%'
                AND  A.SLB_BOOK_NO = B.PTBK_SMPL_BOOK_NO
                AND B.PTBK_COMPANY_NO ='".$data['company']."' ";
            if((trim($data['cancelled']) == true)&& (trim($data['company']) != ''))
                $where .= "( A.SLB_DATE_CANCELLED != '01/01/0001')
                AND  A.SLB_BOOK_NO = B.PTBK_SMPL_BOOK_NO
                AND B.PTBK_COMPANY_NO ='".$data['company']."'";
            if((trim($data['current']) == true)&&(trim($data['company']) != ''))
                $where .= "( A.SLB_DATE_CANCELLED = '01/01/0001')
                AND  A.SLB_BOOK_NO = B.PTBK_SMPL_BOOK_NO
                AND B.PTBK_COMPANY_NO ='".$data['company']."'";

            $query = "SELECT DISTINCT A.SLB_BOOK_NO, A.SLB_DESC,A.SLB_DATE_INTRO FROM DEMO_DAY.SLBOOK AS A,DEMO_DAY.SLPTBK AS B " . $where;
            //echo $query;

            $result = odbc_exec($conn, $query);

            while ($row = odbc_fetch_array($result)) {
            $bookList[] = $row;
            }
            var_dump($bookList);
            if (count($bookList) > 0) {
                $record['result'] = true;
                foreach ($bookList as $bookRecord) {
                    $book['book_number'] = $bookRecord['SLB_BOOK_NO'];
                    $book['book_name'] = $bookRecord['SLB_DESC'];
                    $book['book_intro_date'] = $bookRecord['SLB_DATE_INTRO'];
                    $record['data'][] = $book;
                }

            } else {
                $record['result_code'] = 52;
            }
        } else {
            $record['result_code'] = 51;
        }
        odbc_close($conn);
        return (object)$record;
    }

}