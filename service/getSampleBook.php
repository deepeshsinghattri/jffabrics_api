<?php

class Service extends database_connection
{
    public function doService($data)
    {
        //output json message parameters,
        $record['fault']=false;            //Only indicates application internal issues. For example, if database server is down
        $record['result']=false;           //True if the account was validated, false otherwise.
        $record['fault_code']=0;           //True if the account was validated, false otherwise.
        $record['result_code']=0;          //True if the account was validated, false otherwise.
        //$record['category']='';            //category of user, if result is true, undefined otherwise.
        //$record['jfauth_token']='';        //Set to the token, if result is true, undefined otherwise.

        if(!$this->isKeyExists($data['token'])) {
            $record['result_code']=54;
            return (object)$record;
        }

        $conn = $this->connect();
        if(!$conn){
            $record['fault']=true;
            $record['fault_code']=11;
            return (object)$record;

        }
        $where = '';
        if ((trim($data['book_num']) != '') && (trim($data['company']) != '')) {

            $where = " where A.SLB_BOOK_NO ='" . $data['book_num'] . "' AND B.PTBK_COMPANY_NO ='" . $data['company'] . "'
            AND A.SLB_BOOK_NO = B.PTBK_SMPL_BOOK_NO
	        AND B.PTBK_PATTERN = C.PTPR_PATTERN
	        AND B.PTBK_COLOUR = E.STD_COLOUR
	        AND C.PTPR_PRICE_CODE = D.ARC_PRICE_CODE
	        AND FETCH FIRST 1 ROW ONLY ";

            $query = "SELECT  A.SLB_BOOK_NO, A.SLB_DESC,B.PTBK_PATTERN,C.PTPR_PRICE_01,E.CLF_PURC_UNIT  FROM DEMO_DAY.SLBOOK AS A, DEMO_DAY.SLPTBK AS B ,DEMO_DAY.STPTPR AS C,DEMO_DAY.ARCUST AS D,DEMO_DAY.STCLFL AS E " . $where;
            //echo $query;
            $result = odbc_exec($conn, $query);

            $row = odbc_fetch_array($result);
            //var_dump($row);
            if (trim($row['SLB_BOOK_NO']) != '') {
                $record['result'] = true;
                $record['book_number'] = $row['SLB_BOOK_NO'];
                $record['book_name'] = $row['SLB_BOOK_NO'];
                $record['book_pattern'] = $row['PTBK_PATTERN'];
                $record['retail_price'] = $row['PTPR_PRICE_01']*2;
                $record['unit'] = $row['CLF_PURC_UNIT'];

            }else{
                $record['result_code']=52;
            }
        }else{
            $record['result_code'] = 51;
        }
        odbc_close($conn);
        return (object)$record;
    }

}