<?php

class Service extends database_connection
{
    public function doService($data)
    {
        //output json message parameters,
        $record['fault']=false;            //Only indicates application internal issues. For example, if database server is down
        $record['result']=false;           //True if the account was validated, false otherwise.
        $record['fault_code']=0;           //True if the account was validated, false otherwise.
        $record['result_code']=0;           //True if the account was validated, false otherwise.

        if(!$this->isAuthorizedKeys($data['key'])){
            $record['result_code']=60;      //60 is for authorize key missing
            return (object)$record;
        }

        // this will check whether token exists or not
        if(!$this->isKeyExists($data['token'])){
            $record['result_code']=54;      //54 is for token missing
            return (object)$record;
        }

        $conn = $this->connect();
        if(!$conn){
            $record['fault']=true;
            $record['fault_code']=11;
            return (object)$record;
        }

        $where = '';
        if((trim($data['token'])!='')&&(trim($data['pattern_name'])!='')&&(trim($data['colour_code'])!='')){

            $where = " where A.STD_PATTERN='$data[pattern_name]' AND A.STD_COLOUR='$data[colour_code]' AND B.STL_STOCK_NO = A.STD_STOCK_NO AND B.STL_LOT_NO = C.DYE_LOT_NO";

            $query = "SELECT distinct  A.STD_PATTERN,A.STD_COLOUR, A.STD_STOCK_NO, A.STM_UNIT_DESC, A.STM_TYPE,B.STL_LOT_NO,B.STL_QTY_OH, B.STL_QTY_COMM, C.DYE_MATCH_CODE
                      from DEMO_DAY.STCLFL as A, DEMO_DAY.STLOT as B  , DEMO_DAY.STDYEL as C ".$where;
            $result = odbc_exec($conn, $query);

            $pattern = '';
            while($row = odbc_fetch_array($result)){
                $stocks[] = $row;
            }


            if(count($stocks)>0){
                $record['result'] = true;
                foreach($stocks as $row=>$stock) {
                    $item['piece_number']       = $stock['STL_LOT_NO'];
                    $item['unit_code']          = $stock['STM_UNIT_DESC'];
                    $item['available_stock']    = $stock['STL_QTY_OH']-$stock['STL_QTY_COMM'] ;
                    $record[$stock['DYE_MATCH_CODE']][] = $item  ;
                }
            }else{
                $record['result_code']=52;
            }

        }else{
            $record['result_code']=51;
        }

        odbc_close($conn);
        return (object)$record;
    }
}

?>