<?php

class Service extends database_connection
{
    public function doService($data)
    {
        //output json message parameters,
        $record['fault']=false;            //Only indicates application internal issues. For example, if database server is down
        $record['result']=false;           //True if the account was validated, false otherwise.
        $record['fault_code']=0;           //True if the account was validated, false otherwise.
        $record['result_code']=0;          //True if the account was validated, false otherwise.
        //$record['category']='';            //category of user, if result is true, undefined otherwise.
        //$record['jfauth_token']='';        //Set to the token, if result is true, undefined otherwise.

        if(!$this->isAuthorizedKeys($data['key'])){
            $record['result_code']=60;      //60 is for authorize key missing
            return (object)$record;
        }

        if(!$this->isKeyExists($data['token'])) {
            $record['result_code']=54;
            return (object)$record;
        }

        $conn = $this->connect();
        if(!$conn){
            $record['fault']=true;
            $record['fault_code']=11;
            return (object)$record;
        }
        $where = '';
        if((trim($data['category'])!='')){
            $where = " where PTN_CATEGORY='".$data['category']."'";
            $query = "SELECT PTN_UNIT_DESC AS UNIT_CODE,
                            CASE
                                WHEN PTN_UNIT_DESC = 'YD' THEN 'Yard'
                                WHEN PTN_UNIT_DESC = 'EA' THEN 'Each'
                                WHEN PTN_UNIT_DESC = 'MT' THEN 'Meter'
                                WHEN PTN_UNIT_DESC = 'DR' THEN 'Double Role'
                                WHEN PTN_UNIT_DESC = 'PR' THEN 'Pair'
                                WHEN PTN_UNIT_DESC = 'PK' THEN 'Pack'
                                WHEN PTN_UNIT_DESC = '1F' THEN '1 Foot'
                                WHEN PTN_UNIT_DESC = '2F' THEN '2 Feet'
                            END
 		              AS UNIT_NAME FROM DEMO_DAY.STPATN".$where;
            $result = odbc_exec($conn, $query);

            $row = odbc_fetch_array($result);
            //print_r($row);
            if(trim($row['UNIT_CODE'])!='') {
                $record['result'] = true;
                $record['data']= $row;

            }else {
                //if nothing get fetched
                $record['result_code']=52;
            }

        }else {
            //if input is missing
            $record['result_code']=51;
        }
            return (object)$record;
    }
}