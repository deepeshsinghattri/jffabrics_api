<?php

class Service extends database_connection
{
    public function doService($data)
    {
        //output json message parameters,
        $record['fault']=false;            //Only indicates application internal issues. For example, if database server is down
        $record['result']=false;           //True if the account was validated, false otherwise.
        $record['jfauth_token']='';        //Set to the token, if result is true, undefined otherwise.
        $record['fault_code']=0;           //if there is problem in database connection, default 0 if no issue.
        $record['result_code']=0;           //if there is problem in application, default 0 if no issue.

        if(!$this->isAuthorizedKeys($data['key'])){
            $record['result_code']=60;      //60 is for authorize key missing
            return (object)$record;
        }

        $conn = $this->connect();
        if(!$conn){
            $record['fault']=true;
            $record['fault_code']=11;
            return (object)$record;
        }

        $ranNumber = $this->getRandomNumber();
        $where = '';
        if((trim($data['user_id'])!='')&&(trim($data['password'])!='')){
            $where = " where WEB_USER_ID='".$data['user_id']."' AND WEB_PASSWORD='".$data['password']."'";

            $query = "SELECT WEB_ACCOUNT_NO from DEMO_DAY.WEBUSR ".$where;
            $result = odbc_exec($conn, $query);

            $row = odbc_fetch_array($result);

            if(trim($row['WEB_ACCOUNT_NO'])!=''){
                $record['result'] = true;
                $record['jfauth_token'] = md5($data['user_id'].$ranNumber);

                //set key,value,expiry time for into memcache
                $this->setData($record['jfauth_token'], $data['user_id']);

            }else{
                $record['result_code']=52;
            }

        }else{
            $record['result_code']=51;
        }

        odbc_close($conn);
        return (object)$record;
    }
}

?>