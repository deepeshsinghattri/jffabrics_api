<?php

class Service extends database_connection
{
    public function doService($data)
    {
        //output json message parameters,
        $record['fault'] = false;            //Only indicates application internal issues. For example, if database server is down
        $record['result'] = false;           //True if the account was validated, false otherwise.
        $record['fault_code'] = 0;           //True if the account was validated, false otherwise.
        $record['result_code'] = 0;          //True if the account was validated, false otherwise.
        $record['module_code'] = '';         //Set to the token, if result is true, undefined otherwise.
        $record['message'] = '';             //Set to the token, if result is true, undefined otherwise.

        if(!$this->isAuthorizedKeys($data['key'])){
            $record['result_code']=60;      //60 is for authorize key missing
            return (object)$record;
        }
        // this will check whether token exists or not
        if (!$this->isKeyExists($data['token'])) {
            $record['result_code'] = 54;
            return (object)$record;
        }

        $conn = $this->connect();
        if (!$conn) {
            $record['fault'] = true;
            $record['fault_code'] = 11;
            return (object)$record;
        }
        $where = '';
        if ((trim($data['sku']) != '') && (trim($data['company']) != '') && (trim($data['colour_code']) != '')) {

            $where = " where A.STP_STOCK_NO = B.STD_STOCK_NO AND A.STP_STOCK_NO ='" . $data['sku'] . "' AND A.STP_COMPANY_NO ='" . $data['company'] . "'AND B.STD_COLOUR ='" . $data['colour_code'] . "'";
            $query = "SELECT A.STP_TYPE,A.STP_DESC_1,A.STP_DESC_2,A.STP_DESC_3,A.STP_DESC_4,A.STP_DESC_5,A.STP_DESC_6,A.STP_DESC_7,A.STP_DESC_8
                      FROM DEMO_DAY.STPROF AS A,DEMO_DAY.STCLFL AS B" . $where;
            $result = odbc_exec($conn, $query);

            $row = odbc_fetch_array($result);

            if (trim($row['STP_TYPE']) != '') {
                $record['result'] = true;
                $record['module_code'] = $row['STP_TYPE'];

                for ($i = 1; $i <= 8; $i++) {
                    $desc[$i] = $row['STP_DESC_'.$i];
                    $desc['module_code']= $row['STP_TYPE'];
                }
                $record['message'][] = $desc;

            } else{
                $record['result_code']=52;
            }

        } else{
            $record['result_code'] = 51;
        }

        odbc_close($conn);
        return (object)$record;
    }

}
?>